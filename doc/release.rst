.. highlight:: shell-session

Deployment
==========

Ligo-raven is deployed in gwcelery once a new release is made and depedencies
are updated. Therefore, the focus of deployment is making a release and
getting it approved by the RAVEN review team and the `SCCB`_.

Making a new release
--------------------

#. Firstly, ensure all all changes we want to include are made by double
   checking commits on the master branch and open merge requests. Switch to the
   up-to-date remote master branch via::

        $ git fetch upstream
        $ git checkout master
        $ git rebase upstream/master
   
   You can check you are properly up-to-date by cross-checking the
   `upstream commits`_ to your local log via::

        $ git log

#. Make sure all changes are clearly described in the `CHANGES.rst`_,
   potentially pushing a commit updating this to the master branch if need be.

#. Prepare a release by updating `CHANGES.rst`_ with correct date and version
   number. Update the version number in `pyproject.toml`_ if needed as well.
   You can check you have set the correct version number by running::

        $ cat pyproject.toml | grep 'version ='

#. Make a commit with these changes:: 

        $ git commit -a -m "Update CHANGES.rst"

   and tag this with the version number::

        $ git tag vX.XX -m "Version X.XX"

   where X.XX is the version number. Push these changes via::

        $ git push upstream && git push upstream --tags

   A CI pipeline should soon upload this version during the deploy stage.
   Check this was uploaded correctly by looking at the `PyPI`_ page and the CI
   pipeline of this new tag.

#. Lastly, we can start preparing the next release by creating a new entry in
   `CHANGES.rst`_. This is done by adding the following::

        X.XX (unreleased)
        -----------------

        -   No changes yet
    
   where X.XX is the next version number. Also update `pyproject.toml`_ with
   this new version number. Then push this to remote repository::

        $ git commit -a -m "Back to development"
        $ git push upstream

Initiating review
-----------------

If a release is intended to run online during an operting run via `gwcelery`_,
this release must be reviewed first.

The RAVEN review team should be
contacted and some sort of review process arranged, whether via email or
calls. The members of this review team generally review pipelines under the
umbrella of the GRB group, so being involved with the group and knowning the
members is important.
This process may take some time and may require another release if
changes are requested, so plan accordingly.

After this has been approved by the RAVEN review team, an `SCCB`_ request
should be made, filling out the appropriate info and responding to the
reviewers on the issue if prompted. Once this request has been approved, the
release is all set to run in `gwcelery`_ on live data.

.. _`CHANGES.rst`: https://git.ligo.org/lscsoft/raven/-/blob/master/CHANGES.rst
.. _`gwcelery`: https://igwn.readthedocs.io/projects/gwcelery/en/latest/
.. _`PyPI`: https://pypi.org/project/ligo-raven/
.. _`SCCB`: https://git.ligo.org/computing/sccb/-/issues/new
.. _`pyproject.toml`: https://git.ligo.org/lscsoft/raven/-/blob/master/pyproject.toml
.. _`upstream commits`: https://git.ligo.org/lscsoft/raven/-/commits/master?ref_type=heads
