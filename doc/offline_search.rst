.. highlight:: shell-session

Offline search
==============

Ligo-raven also now has tools to run searches in an offline mode. The inputs
can either be any combination of a GraceDB server and/or local files. This
allow us to perform the following example searches:

1. Compare GWs and external events in the same GraceDB server to check the
   results of online RAVEN after an operating run.

2. Compare a list of new triggers not used in online search to those in a
   server, such as providing a list of fast radio bursts to see if there
   are any coincidences with GW candidates.

3. Compare two lists of offline triggers to perform a simulation of a search
   or test that the methods in RAVEN give sensible results, such as random
   coincidences giving an expected joint FAR distribution.

Running the script
------------------

Similar to other functions in ligo-raven, the offline search can be run in a
python script or via the terminal, where a latter example for the CBC-GRB
search for O4a is::

    $ raven_offline_search -i https://gracedb.ligo.org/api/ https://gracedb.ligo.org/api/ -o results_O4a_CBC -t 1368994979.84 1389072434.67 -w -1 5 -g CBC -s GRB -n 4

For a full list of options see
:doc:`bin.raven_offline_search <bin.raven_offline_search>`.
Also note the use GraceDB API urls here, see :doc:`examples <examples>` for
more options. We can also use local files as well.

Example local file inputs
^^^^^^^^^^^^^^^^^^^^^^^^^

We use `Astropy Tables`_ to load local files into a mock version of GraceDB,
which support `various file formats`_. Please see the following as examples of
`.csv` inputs for GW candidates and external candidates respectively 

.. csv-table:: Example GW injection file
    :header-rows: 1
    :file: ../ligo/raven/tests/data/GW170817/test-gw-search.csv
    :widths: 15, 15, 4, 8, 6, 5, 5, 20
    :class: longtable

.. csv-table:: Example GRB injection file
    :header-rows: 1
    :file: ../ligo/raven/tests/data/GW170817/test-grb-search.csv
    :widths: 7, 10, 8, 16, 5, 3, 4, 12, 20
    :class: longtable

Interpreting results
--------------------

Since examining outputs of this search can be tricky, let's learn how to do
this while looking an example.

Example of background
^^^^^^^^^^^^^^^^^^^^^

Here, we constructed an injection
set of random coincidences, setting random times and sky maps (so no shared
injection parameters), as well as GW FARs consistent with background. We see
this in the following plot:

.. image:: _static/Gravitational_far.png
   :alt: GW FAR input
   :scale: 100 %

This altogether should give the result of a joint FAR consistent with the
expected background, which we see in the following temporal joint FAR plot:

.. image:: _static/Coincidence_far.png
   :alt: Example output
   :scale: 100 %

Zooming out and also including the joint FAR with spatial information we get:

.. image:: _static/all_far_background.png
   :alt: Example output
   :scale: 75 %

where we see the skew that can exist for this type of joint FAR. This is a
known issue and is mostly relevant for low-significance joint candidates.

Example including foreground
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Crucially important to point out is that real searches with foreground
candidates will have an excess in the high-significance regime compared to
the expected background. Please see the following for an example with GW
candidates:

.. image:: _static/Gravitational_far_real.png
   :alt: O4a GW CBC FAR
   :scale: 100 %

If a similar curve is observed for joint candidates, this excess could be
evidence of a population of real coincidences, even sub-threshold, and could
be of interest along with highly-significant events.

Alert thresholds
^^^^^^^^^^^^^^^^

Of particular interest when running offline searches is finding highly
significant events that could be of interest to the astronomical community.
An alert threshold is a variable set when launching the offline search and a
check is done whether the joint FAR with sky map info passes this, recorded
in the column `passes_threshold` shown in this truncated results file:

.. csv-table:: Example results file
    :header-rows: 1
    :file: _static/results.csv
    :widths: auto
    :class: longtable

Note that if you are using a GraceDB instance and you find a new significant
joint candidate(s), please check the appropriate event in that GraceDB instance
to check if there was a good reason why the joint candidate was rejected by the
online RAVEN search. The `previously_found` and `grb_not_real` columns can
help with this as well.

If a joint candidate(s) passes reasonable thresholds and does not have obvious
reasons to veto or was previously known, it may be worth sharing with the
astronomical community.

.. _`Astropy Tables`: https://docs.astropy.org/en/stable/table/index.html
.. _`various file formats`: https://docs.astropy.org/en/stable/table/io.html#supported-formats